#include <QtGui/QGuiApplication>
#include <QQuickView>
#include <QUrl>

#include "viewer.hpp"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    Viewer viewer;
    viewer.setSource(QUrl::fromLocalFile("qml/main.qml"));
    viewer.show();

    return app.exec();
}
