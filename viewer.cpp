/* viewer.cpp created 5/27/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "viewer.hpp"

#include <QResizeEvent>
#include <QQuickItem>
#include <QString>
#include <QSize>

#include <iostream>

Viewer::Viewer() : QtQuick2ApplicationViewer() {}

Viewer::~Viewer() {}

void Viewer::resizeEvent(QResizeEvent* event) {
  QtQuick2ApplicationViewer::resizeEvent(event);
  if(rootObject()->state() == "landscape") {
    if(event->size().height() >= event->size().width()) {
      rootObject()->setState(QString("portrait"));
      std::cout << "Resize Event! Width: " << event->size().width() << " Height: " << event->size().height() << std::endl;
    }
  } else if(event->size().height() < event->size().width()) {
    rootObject()->setState(QString("landscape"));
    std::cout << "Resize Event! Width: " << event->size().width() << " Height: " << event->size().height() << std::endl;
  }
}
