import QtQuick 2.0

Item {
  id: root
  /*
  function startup() {

    console.debug(root.height + " " + root.width)
    if(root.height >= root.width)
      root.state = "portrait"
    else
      root.state = "landscape"
  }

  Component.onCompleted: startup();
  */
  Rectangle {
    id: blue
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    height: parent.height / 2

    //Behavior on width { SmoothedAnimation { velocity: 100 } }
  }

  Rectangle {
    id: orange
    anchors.right: parent.right
    anchors.top: parent.top
    height: parent.height / 2

    //Behavior on width { SmoothedAnimation { velocity: 100 } }
  }

  MouseArea {
    anchors.fill: parent
    onClicked: Qt.quit()
  }

  states : [
    State {
      name: "portrait"
      PropertyChanges { target: blue; width: root.width; color: "lightcyan" }
      PropertyChanges { target: orange; width: root.width; color: "lightsalmon" }
    },
    State {
      name: "landscape"
      PropertyChanges { target: blue; width: root.width / 2; color: "midnightblue" }
      PropertyChanges { target: orange; width: root.width / 2; color: "darkorange" }
    }
  ]

/* Warning! There are some gotchas in transitions I've found out:
  1. You can't specify multiple "from" or "to" targets, you need seperate transitions for each.
  2. You can't specify target.property in properties, you explicitly need to set targets.
  3. It is buggy behavior to define animations and include them in transitions. */
  transitions: [
    Transition {
      from: "landscape"
      SmoothedAnimation {
        targets: [orange, blue]
        property: "width"
        velocity: 500
      }
      ColorAnimation {
        targets: [orange, blue]
        duration: 1000
      }
    },
    Transition {
      from: "portrait"
      SmoothedAnimation {
        targets: [orange, blue]
        property: "width"
        velocity: 500
      }
      ColorAnimation {
        targets: [orange, blue]
        duration: 1000
      }
    }
  ]
}
