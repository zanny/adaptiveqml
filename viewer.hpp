/* viewer.hpp created 5/28/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VIEWER_HPP
#define VIEWER_HPP

#include <QResizeEvent>
#include <QObject>

#include "qtquick2applicationviewer.h"

class Viewer : public QtQuick2ApplicationViewer {
Q_OBJECT
public:
  Viewer();
  virtual ~Viewer();
  void resizeEvent(QResizeEvent*) override;
};

#endif // VIEWER_HPP
